var MyContract;

var abi = [{
    "constant": false,
    "inputs": [{"name": "payOutAddress", "type": "address"}, {"name": "inHowManyRates", "type": "uint256"}],
    "name": "withDraw",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "die",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "payOutAddr", "type": "address"}],
    "name": "calculateFee",
    "outputs": [{"name": "", "type": "uint256"}],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "payOutAddr", "type": "address"}],
    "name": "enroll",
    "outputs": [],
    "payable": true,
    "stateMutability": "payable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "reproccess",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "payOutAddr", "type": "address"}],
    "name": "deposit",
    "outputs": [],
    "payable": true,
    "stateMutability": "payable",
    "type": "function"
}, {"inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor"}, {
    "anonymous": false,
    "inputs": [{"indexed": false, "name": "payoutAddress", "type": "address"}, {
        "indexed": false,
        "name": "sender",
        "type": "address"
    }, {"indexed": false, "name": "dateOfTransaction", "type": "uint256"}],
    "name": "paymentAccepted",
    "type": "event"
}, {
    "anonymous": false,
    "inputs": [{"indexed": false, "name": "payoutAddress", "type": "address"}, {
        "indexed": false,
        "name": "sender",
        "type": "address"
    }, {"indexed": false, "name": "dateOfTransaction", "type": "uint256"}],
    "name": "enrolled",
    "type": "event"
}, {
    "anonymous": false,
    "inputs": [{"indexed": false, "name": "adr", "type": "address"}, {
        "indexed": false,
        "name": "amount",
        "type": "uint256"
    }, {"indexed": false, "name": "date", "type": "uint256"}, {"indexed": false, "name": "paycount", "type": "int256"}],
    "name": "checker",
    "type": "event"
}];
var contractAddress = '0x358365494337FfdcD6016BD71112907eAfD7c2Dc';
var myContractInstance;
console.log(contractAddress);

window.addEventListener('load', function () {
    if (typeof web3 !== 'undefined') {
        window.web3 = new Web3(web3.currentProvider);
/*
        MyContract = web3.eth.contract(abi);
        myContractInstance = MyContract.at(contractAddress);
*/

    } else {
        console.log('No web3? You should consider trying MetaMask!');
        window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    }
    //startApp()

});

MyContract = web3.eth.contract(abi);
myContractInstance = MyContract.at(contractAddress);


//validate if web3 injected

function joinTont() {
    var addr = document.getElementById('addrToEnroll').value;

    console.log(addr);

    myContractInstance.enroll.sendTransaction(addr,
        {
            from: web3.eth.accounts[0],
            to: contractAddress,
            value: web3.toWei('0.1', 'ether'),
            gas: 2000000
        },
        function (err, transactionHash) {
            if (!err) {
                console.log(transactionHash);
            }
        });

}

function payment() {
    var addr = document.getElementById('addrToPayment').value;
    var eth = document.getElementById('valueOfPayment').value;

    myContractInstance.deposit.sendTransaction(addr,
        {
            from: web3.eth.accounts[0],
            to: contractAddress,
            value: web3.toWei(eth, 'ether'),
            gas: 2000000
        },
        function (err, transactionHash) {
            if (!err) {
                console.log(transactionHash);
            }
        });
}

function payout() {
    var addr = document.getElementById('addrToPayout').value;
    var numberOfPays = document.getElementById('numberOfPays').value;

    myContractInstance.withDraw.sendTransaction(addr, numberOfPays,
        {
            from: web3.eth.accounts[0],
            to: contractAddress,
            gas: 2000000
        },
        function (err, transactionHash) {
            if (!err) {
                console.log(transactionHash);
            }
        });
}

function repo() {
    myContractInstance.reproccess.sendTransaction(
        {
            from: web3.eth.accounts[0],
            to: contractAddress,
            gas: 2000000
        },
        function (err, transactionHash) {
            if (!err) {
                console.log(transactionHash);
            }
        });
}